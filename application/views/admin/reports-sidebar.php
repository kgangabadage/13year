<?php
/**
 * Created by Kosala.
 * email: kosala4@gmail.com
 * Date: 9/26/17
 * Time: 1:54 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?php echo base_url()."index.php/admin/index"?>" id="adminHome" data-admin="admin">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle" id="subjectsMenu">
                        <i class="material-icons">book</i>
                        <span>Select Subject</span>
                    </a>
                    <ul class="ml-menu" id="subjectListMenu">
                    <input type="text" class="form-control" id="searchSubjects" placeholder="Search Subjects" title="Type in a name">

                    <?php if ($subjects) { ?>
                    <?php foreach ($subjects as $row) { ?>
                        <li>
                            <a href="javascript:void(0);" data-id="<?php echo $row['id'];?>" data-name = "<?php echo $row['subject_name'] ;?>" data-type="subject" class="searchBySubject filter">
                                <span> <?php echo $row['subject_name'] ;?> </span>
                            </a>
                        </li>
                    <?php    } ?>
                    <?php } ?>

                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" id="resetMenu" class="reset">
                        <i class="material-icons">repeat</i>
                        <span>Reset</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->

        <?php $this->load->view('footerNote'); ?>

        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
    </aside>
    <!-- #END# Right Sidebar -->
</section>
