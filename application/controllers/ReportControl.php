<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# @Author: Kosala Gangabadage
# @Date:   2017-10-25T14:50:29+05:30
# @Email:  kosala4@gmail.com
# @Last modified by:   Kosala Gangabadage
# @Last modified time: 2018-01-17T12:25:31+05:30

class ReportControl extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Form_data_model'); //load database model.
        $this->load->model('Report_data_model'); //load database model.
        $this->load->model('User_model'); //load database model.

    }

    public function check_sess()
    {
        $Role = $this->session->user_role;
        if ($this->session->user_logged == "in") {

        } elseif ($Role == '0'){

        } else {
            $this->logout(); //Redirect to login page if session not initiated.
        }
    }

    //Logout function
    function logout()
    {
        $this->session->sess_destroy();
        redirect('/login/index');
    }

    public function getSchoolsByPhase(){

        header('Content-Type: application/x-json; charset=utf-8');
        $phase = $this->security->xss_clean($this->input->post('phase'));
        $res = $this->Report_data_model->getSchoolsByPhase($phase);

        $jsonData['schools'] = count($res);
        $jsonData['teachers'] = $this->Report_data_model->getTeachersByPhase($phase, 'count');
        $jsonData['classes'] = $this->Report_data_model->getClassesByPhase($phase, 'count');
        $jsonData['students'] = $this->Report_data_model->getStudentsByPhase($phase, 'count', 'all');

        $row = array();
        foreach ($res as $rowIndex => $r){
            unset($row);
            foreach ($r as $colIndex => $c){
                if ($rowIndex == '0') {
                    $jsonData['columns'][] = array('title' => $colIndex, 'data' => $colIndex);
                    $row[$colIndex] = ucwords($c);
                } else {
                    $row[$colIndex] = ucwords($c);
                }
            }
            $jsonData['data'][] = $row;
        }
        echo json_encode($jsonData);
    }

    public function getSchoolsBySubject(){

        header('Content-Type: application/x-json; charset=utf-8');
        $subject_id = $this->security->xss_clean($this->input->post('subject_id'));
        $res = $this->Report_data_model->getSchoolsBySubject($subject_id);

        $jsonData['schools'] = count($res);
        $jsonData['teachers'] = $this->Report_data_model->getSubjectTeachers($subject_id, 'count');
        $jsonData['classes'] = $this->Report_data_model->getSubjectClasses($subject_id, 'count');
        $jsonData['students'] = $this->Report_data_model->getSubjectStudents($subject_id, 'count', 'all');

        $row = array();
        foreach ($res as $rowIndex => $r){
            unset($row);
            foreach ($r as $colIndex => $c){
                if ($rowIndex == '0') {
                    $jsonData['columns'][] = array('title' => $colIndex, 'data' => $colIndex);
                    $row[$colIndex] = ucwords($c);
                } else {
                    $row[$colIndex] = ucwords($c);
                }
            }
            $jsonData['data'][] = $row;
        }

        echo json_encode($jsonData);
    }

    public function getSchoolsByProvince(){

        header('Content-Type: application/x-json; charset=utf-8');
        $province_id = $this->security->xss_clean($this->input->post('province_id'));
        $res = $this->Report_data_model->getSchoolsByProvince($province_id);

        $jsonData['schools'] = count($res);
        $jsonData['teachers'] = $this->Report_data_model->getSubjectTeachers($subject_id, 'count');
        $jsonData['classes'] = $this->Report_data_model->getSubjectClasses($subject_id, 'count');
        $jsonData['students'] = $this->Report_data_model->getSubjectStudents($subject_id, 'count', 'all');

        $row = array();
        foreach ($res as $rowIndex => $r){
            unset($row);
            foreach ($r as $colIndex => $c){
                if ($rowIndex == '0') {
                    $jsonData['columns'][] = array('title' => $colIndex, 'data' => $colIndex);
                    $row[$colIndex] = ucwords($c);
                } else {
                    $row[$colIndex] = ucwords($c);
                }
            }
            $jsonData['data'][] = $row;
        }

        echo json_encode($jsonData);
    }

    public function getAllSchools(){
        header('Content-Type: application/x-json; charset=utf-8');
        $phase = $this->security->xss_clean($this->input->post('phase'));
        $res = $this->Report_data_model->getAllSchools();

        $jsonData = array();
        $row = array();
        foreach ($res as $rowIndex => $r){
            unset($row);
            foreach ($r as $colIndex => $c){
                if ($rowIndex == '0') {
                    $jsonData['columns'][] = array('title' => $colIndex, 'data' => $colIndex);
                    $row[$colIndex] = ucwords($c);
                } else {
                    $row[$colIndex] = ucwords($c);
                }
            }
            $jsonData['data'][] = $row;
        }
        echo json_encode($jsonData);
    }

    public function getZones(){
        header('Content-Type: application/x-json; charset=utf-8');
        $district_id = $this->security->xss_clean($this->input->post('district_id'));
        $res = $this->Form_data_model->get_zones($district_id);
        echo json_encode($res);
    }

    public function getTeachers(){
        header('Content-Type: application/x-json; charset=utf-8');
        $subject_id = $this->security->xss_clean($this->input->post('subj'));
        $res = $this->Form_data_model->getTeachersForSubjects($subject_id, $this->session->school_id);
        echo json_encode($res);
    }

    public function getTeachers_School(){
        header('Content-Type: application/x-json; charset=utf-8');
        $school_id = $this->session->school_id;
        $search_array = array('school_id'=> $school_id);
        $res = $this->Form_data_model->searchdb('teachers', $search_array);
        echo json_encode($res);
    }

    public function getSchoolData(){
        header('Content-Type: application/x-json; charset=utf-8');
        $school_id = ($this->input->post('school_id'));
        $search_type = ($this->input->post('search_type'));

        switch ($search_type) {
            case 'teachers':
                $res = $this->Report_data_model->getSchoolTeachers($school_id, 'list');
                break;

            case 'classes':
                $res = $this->Report_data_model->getSchoolClasses($school_id, 'list');
                break;

            case 'students':
                $res = $this->Report_data_model->getSchoolStudents($school_id, 'list', 'all');
                break;

            case 'male students':
                $res = $this->Report_data_model->getSchoolStudents($school_id, 'list', 'Male');
                break;

            case 'female students':
                $res = $this->Report_data_model->getSchoolStudents($school_id, 'list', 'Female');
                break;

            case 'funds':
                $res = $this->Report_data_model->getSchoolFunds($school_id, 'list');
                break;

            default:
            $res = '';
                break;
        }

        $jsonData = array();
        $row = array();
        foreach ($res as $rowIndex => $r){
            unset($row);
            foreach ($r as $colIndex => $c){
                if ($rowIndex == '0') {
                    $jsonData['columns'][] = array('title' => $colIndex, 'data' => $colIndex);
                    $row[$colIndex] = ucwords($c);
                } else {
                    $row[$colIndex] = ucwords($c);
                }
            }
            $jsonData['data'][] = $row;
        }
        echo json_encode($jsonData);
    }
}
