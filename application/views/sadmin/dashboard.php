<?php
/**
 * Created by Kosala.
 * email: kosala4@gmail.com
 * User: edu
 * Date: 9/27/17
 * Time: 2:58 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="<?php echo base_url()."assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"?>" rel="stylesheet" />

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2> <?php echo strtoupper($school['0']['schoolname']);?> </h2>
        </div>
    <?php if ($this->session->flashdata('success')){ ?>
        <div class="row">
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        <?php } else if ($this->session->flashdata('not-success')){ ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata('not-success'); ?>
            </div>
        </div>
    <?php } ?>
        <!-- Teacher List Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            TEACHERS LIST
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="teachers">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Name</th>
                                    <th>NIC No</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Subject 01</th>
                                    <th>Subject 02</th>
                                    <th>Subject 03</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- Class List Table -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="card">
                    <div class="header">
                        <h2>
                            CLASSES LIST
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="classes">
                                <thead>
                                    <tr>
                                        <th>Grade</th>
                                        <th>Class</th>
                                        <th>Class Teacher</th>
                                        <th>Class Start Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Subjects List Table -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="float: right;">
                <div class="card">
                    <div class="header">
                        <h2>
                            SELECTED SUBJECTS LIST
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="subjects">
                                <thead>
                                    <tr>
                                        <th>Class</th>
                                        <th>Subject Name</th>
                                        <th>Teacher</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- Users List Table -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="card">
                    <div class="header ">
                        <h2>
                            USERS LIST
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="users">
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th> User Name </th>
                                        <th> Role </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Students List Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            STUDENTS LIST
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="students">
                                <thead>
                                    <tr>
                                        <th>Index No</th>
                                        <th>Name with Initials</th>
                                        <th>NIC</th>
                                        <th>gender</th>
                                        <th>address</th>
                                        <th>Parent Telephone</th>
                                        <th>Medium</th>
                                        <th>Distance to school (km)</th>
                                        <th>Parent Income</th>
                                        <th>Travel Mode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Teachers Modal -->
    <div id="TeachersModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="TeachersModal-title">  </h4>
                </div>

                <?php echo form_open('sadmin/Teachers', 'role="form" id="TeachersForm"') ?>
                <div class="modal-body">
                    <input type="text" class="hidden" name="teacher_id" id="teacher_id">
                    <input type="hidden" class="form-control" name="formAction" id="teachersFormAction">
                    <div class="row clearfix">
                        <div class="col-md-6">
                            <!--<label for="name" class="required">Teacher Name</label>-->
                            <div class="form-group form-float">
                                <label class="form-label required">Title</label>
                                <select id="title" class="form-control show-tick" name="title" required>
                                <option value="">-- Please select --</option>
                                <option value="Rev.">Rev. </option>
                                <option value="Mr.">Mr. </option>
                                <option value="Mrs.">Mrs. </option>
                                <option value="Ms.">Ms. </option>
                            </select>
                            </div>
                            <label for="name" class="required">Teacher Name</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="name" id="teacherName" required>
                                    <label class="form-label">Teacher Name With Initials</label>
                                </div>
                            </div>
                            <label for="nic" class="required">NIC</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="nic" id="teacherNIC" required>
                                    <label class="form-label">Teacher NIC No</label>
                                </div>
                            </div>
                            <label for="dob" class="required">Birth Day</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="dob" id="teacherDOB" required>
                                    <label class="form-label">Birth Day</label>
                                </div>
                            </div>
                            <label for="telephone" class="required">Mobile No</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control telephone" name="mobile" id="teacherMobile" placeholder="Ex: 000-1234567">
                                </div>
                            </div>
                            <label for="email">Email Address</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" class="form-control" name="email" id="teacherEmail">
                                    <label class="form-label">Email Address</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-float">
                                <label class="form-label required">Subject 1</label>
                                <select id="sub1" class="form-control show-tick" name="sub1" required data-live-search="true">
                                    <option value="">-- Please select --</option>

                                    <?php if ($subjects) { ?>
                                    <?php foreach ($subjects as $row) { ?>
                                    <option value="<?php echo $row['id'];?>" > <?php echo $row['subject_name'] ;?> </option>
                                    <?php    } ?>
                                    <?php } ?>
                                </select>
                                <input type="checkbox" name="trained1" id="trained1" value="1" />
                                <label for="trained1"> Teacher has Trained </label>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label ">Subject 2</label>
                                <select id="sub2" class="form-control show-tick" name="sub2" data-live-search="true">
                                    <option value="">-- Please select --</option>

                                    <?php if ($subjects) { ?>
                                    <?php foreach ($subjects as $row) { ?>
                                    <option value="<?php echo $row['id'];?>" > <?php echo $row['subject_name'] ;?> </option>
                                    <?php    } ?>
                                    <?php } ?>
                                </select>
                                <input type="checkbox" name="trained2" id="trained2" value="1" />
                                <label for="trained2"> Teacher has Trained </label>
                            </div>
                            <div class="form-group form-float">
                                <label class="form-label ">Subject 3</label>
                                <select id="sub3" class="form-control show-tick" name="sub3" data-live-search="true">
                                    <option value="">-- Please select --</option>

                                    <?php if ($subjects) { ?>
                                    <?php foreach ($subjects as $row) { ?>
                                    <option value="<?php echo $row['id'];?>" > <?php echo $row['subject_name'] ;?> </option>
                                    <?php    } ?>
                                    <?php } ?>
                                </select>
                                <input type="checkbox" name="trained3" id="trained3" value="1" />
                                <label for="trained3"> Teacher has Trained </label>
                            </div>
                            <label for="app_ser" class="required">Date Join the Service</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="app_ser" id="teacherApp_ser" required>
                                    <label class="form-label">Date Join the Service</label>
                                </div>
                            </div>
                            <label for="app_sch" class="required">Date Join the School</label>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="datepicker form-control" name="app_sch" id="teacherApp_sch" required>
                                    <label class="form-label">Date Join the School</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer" style="border-top:0;">
                    <button type="submit" class="btn btn-success" id="TeachersModal_submit"> Submit </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
                </div>
                <?php echo form_close() ?>
            </div>

        </div>
    </div>

    <!-- Classes Modal -->
    <div id="ClassesModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="ClassesModal-title">  </h4>
                </div>

                <?php echo form_open('sadmin/Classes', 'role="form" id="ClassesForm"') ?>
                <div class="modal-body">
                    <input type="text" class="hidden" name="class_id" id="class_id" >
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="name">Census ID</label>
                                    <input type="text" class="form-control" name="census_id" id="census_id_class" value="<?php echo $this->session->school_id; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label for="name">Grade</label>
                                <select id="grade_class" class="form-control show-tick" name="grade_class" required>
                                    <option value="">-- Please select --</option>
                                    <option value="Grade 12"> Grade 12 </option>
                                    <option value="Grade 13"> Grade 13 </option>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="name"> Class Name </label>
                                    <input id="name_class" type="text" class="form-control" name="name_class" required>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label for="name"> Class Teacher </label>
                                <select id="teacher_class" class="select2 form-control show-tick teacher_list" name="teacher_class" required>
                                    <option value="">-- Please select --</option>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="name"> Class Start Date </label>
                                    <input id="stdate_class" type="text" class="datepicker form-control" name="stdate_class" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top:0;">
                    <button type="button" class="btn btn-success" id="ClassesModal_submit"> Submit </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
                </div>
                <?php echo form_close() ?>
            </div>

        </div>
    </div>

    <!-- Class Subjects Modal -->
    <div id="SubjectsModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="SubjectsModal-title">  </h4>
                </div>

                <?php echo form_open('sadmin/Subjects', 'role="form" id="SubjectsForm"') ?>
                <div class="modal-body">
                    <input type="text" class="hidden" name="class_subj_id" id="class_subj_id" >
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="name">Census ID</label>
                                    <input type="text" class="form-control" name="census_id" id="census_id_classSubject" value="<?php echo $this->session->school_id; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label for="name">Class</label>
                                <select id="classsubjest_class" class="form-control show-tick" name="classsubjest_class" required>
                                    <option value=""> -- Please select -- </option>
                                    <?php if ($classes) { ?>
                                    <?php foreach ($classes as $row) { ?>
                                        <option value="<?php echo $row['id'];?>"><?php echo  $row['class_name'] ;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <label for="name"> Subject </label>
                                <select id="classsubjest_subject" class="select2 form-control show-tick" name="classsubjest_subject" required>
                                    <option value=""> -- Please select -- </option>
                                    <?php if ($subjects) { ?>
                                    <?php foreach ($subjects as $row) { ?>
                                        <option value="<?php echo $row['id'];?>"><?php echo  $row['subject_name'] ;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group form-float">
                                <label for="name"> Teacher </label>
                                <select id="classsubjest_teacher" class="select2 form-control show-tick teacher_list" name="classsubjest_teacher" required>
                                    <option value=""> -- Please select -- </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top:0;">
                    <button type="button" class="btn btn-success" id="SubjectsModal_submit"> Submit </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
                </div>
                <?php echo form_close() ?>
            </div>

        </div>
    </div>

    <!-- User Modal -->
    <div id="userModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" id="profileModalLabel">Edit User Details</h3>
                </div>
                <div class="modal-body">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#user_edit" data-toggle="tab">
                                 EDIT USER DETAILS
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#user_passwd_change" data-toggle="tab">
                                 CHANGE PASSWORD
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="user_edit">
                            <?php echo form_open('sadmin/users', 'role="form" id="userEditForm"'); ?>
                            <input type="hidden" class="form-control users_user_id" name="users_user_id" id="users_user_id" >
                            <input type="hidden" class="form-control" name="formAction" value="details" >
                            <div class="row clearfix">
                                <div class="col-md-3 form-control-label p-l-0">
                                    <label >Name </label>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="users_name" name="users_name" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3 form-control-label p-l-0">
                                    <label > User Name </label>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="users_username" name="users_username" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3 form-control-label p-l-0">
                                    <label> Role </label>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="form-group form-float">
                                        <select id="users_role" class="form-control show-tick" name="users_role" required>
                                            <option value=""> -- Please select -- </option>
                                            <option value="2"> Finance Administrator </option>
                                            <option value="3"> Class Teacher </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix align-right">
                                  <button type="button" class="btn btn-success waves-effect waves-float users_submit" data-action="details" id="submit_userEditForm" > UPDATE </button>
                                  <!-- <button type="submit" class="btn btn-success waves-effect waves-float" data-action="details" > UPDATE </button> -->
                            </div>
                            <?php echo form_close()?>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="user_passwd_change">
                            <?php echo form_open('', 'role="form" id="user_passwdResetForm"'); ?>
                            <input type="hidden" class="form-control users_user_id" name="users_user_id" >
                            <input type="hidden" class="form-control" name="formAction" value="details" >
                            <div class="row clearfix">
                                <div class="col-md-3 form-control-label p-l-0">
                                    <label> New Password </label>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name="user_passwd" id="user_passwd" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3 form-control-label p-l-0">
                                    <label> ReType Password </label>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name ="user_repasswd" id="user_repasswd" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix align-right">
                                <button type="button" class="btn btn-success waves-effect users_submit" data-action="password" id="submit_user_passwdResetForm" > UPDATE </button>
                            </div>
                            <?php echo form_close()?>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect bg-teal" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/jquery.dataTables.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/jszip.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/select/js/dataTables.select.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/multi-select/js/jquery.multi-select.js"?>"></script>

<!-- Select Plugin Js -->
<script src="<?php echo base_url()."assets/plugins/bootstrap-select/js/bootstrap-select.js"?>"></script>

<!-- Input Mask Plugin Js -->
<script src="<?php echo base_url()."assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"?>"></script>


<script>
    $(document).ready(function () {
        getTeachers();

        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'pdf', 'print'
            ]
        });

        var teachersTable = $('#teachers').DataTable( {
            dom: "Bfrtip",
            responsive: true,
            ajax: {
                url: "<?php echo base_url().'index.php/Sadmin/Dtable/Teachers' ?>",
                data:{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' },
                type: "POST"
            },
            serverSide: true,
            columns: [
                { data: "teachers.title" },
                { data: "teachers.teacher_in_name" },
                { data: "teachers.nic" },
                { data: "teachers.teacher_mobile" },
                { data: "teachers.teacher_email" },
                { data: "subject1.subject_name" },
                { data: "subject2.subject_name" },
                { data: "subject3.subject_name" }
            ],
            select: true,
            buttons: [
                {
                    text: 'Edit',
                    className: 'btn btn-primary waves-effect',
                    action: function ( e, dt, node, config ) {
                        if(teachersTable.rows({selected: true}).data()['0']){
                            $('#TeachersModal-title').text('Edit Teacher Details');
                            $('#TeachersModal').modal('toggle');
                            $('#TeachersForm').validate({
                                rules: {
                                    title : 'required',
                                    name : 'required',
                                    nic : 'required',
                                    dob : 'required',
                                    sub1 : 'required',
                                    app_ser : 'required',
                                    app_sch : 'required',
                                },
                                highlight: function(input) {
                                    $(input).parents('.form-line').addClass('error');
                                },
                                unhighlight: function(input) {
                                    $(input).parents('.form-line').removeClass('error');
                                },
                                errorPlacement: function(error, element) {
                                    $(element).parents('.form-group').append(error);
                                }
                            });

                            var data = teachersTable.rows({selected: true}).data();
                            $('#teachersFormAction').val('edit');
                            $('#teacher_id').val( data[0]['DT_RowId'].split("_")[1] );

                            $('#title').val(data['0']['teachers']['title']).trigger('change');
                            $('#teacherName').val( data['0']['teachers']['teacher_in_name'] ).trigger('change');
                            $('#teacherNIC').val(data['0']['teachers']['nic']).trigger('change');
                            $('#teacherDOB').val( data['0']['teachers']['dob'] ).trigger('change');
                            $('#teacherMobile').val(data['0']['teachers']['teacher_mobile']).trigger('change');
                            $('#teacherEmail').val( data['0']['teachers']['teacher_email'] ).trigger('change');
                            $('#sub1').val(data['0']['teachers']['teacher_sub_1']).trigger('change');
                            $('#sub2').val(data['0']['teachers']['teacher_sub_2']).trigger('change');
                            $('#sub3').val(data['0']['teachers']['teacher_sub_3']).trigger('change');
                            $('#trained1').val( data['0']['teachers']['teacher_trained_1'] ).trigger('change');
                            $('#trained2').val( data['0']['teachers']['teacher_trained_2'] ).trigger('change');
                            $('#trained3').val( data['0']['teachers']['teacher_trained_3'] ).trigger('change');
                            $('#teacherApp_ser').val(data['0']['teachers']['app_date_service']).trigger('change');
                            $('#teacherApp_sch').val( data['0']['teachers']['app_date_school'] ).trigger('change');

                            $('.form-line').addClass('focused');
                        }

                    }
                },
                {
                    text: 'Delete',
                    className: 'btn btn-warning waves-effect',
                    action: function ( e, dt, node, config ) {
                        if(teachersTable.rows({selected: true}).data()['0']){

                            var data = teachersTable.rows({selected: true}).data();
                            var teacher_id = data[0]['DT_RowId'].split("_")[1];
                            var formAction = 'delete';
                            var form_data = new FormData();
                            var post_url = "index.php/sadmin/Teachers/2";

                            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
                            form_data.append('formAction', formAction);
                            form_data.append('teacher_id', teacher_id);

                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>" + post_url,
                                dataType :'text',
                                data: form_data,
                                contentType: false,
                                processData: false,
                                success: function(response){
                                    location.reload();
                                },
                                error: function (response) {
                                    alert("Error Updating! Please try again.");
                                }
                            });
                        }

                    }
                }
            ]
        } );

        var clasTable = $('#classes').DataTable( {
            dom: "Bfrtip",
            responsive: true,
            ajax: {
                url: "<?php echo base_url().'index.php/Sadmin/Dtable/Classes' ?>",
                data:{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' },
                type: "POST"
            },
            serverSide: true,
            columns: [
                { data: "classes.grade" },
                { data: "classes.class_name" },
                { data: "teachers.teacher_in_name" },
                { data: "classes.commenced_date" }
            ],
            select: true,
            bFilter: false,
            buttons: [
                {
                    text: 'New',
                    className: 'btn btn-success waves-effect',
                    action: function ( e, dt, node, config ) {
                        $('#ClassesModal-title').text('Add Class');
                        $('#ClassesModal_submit').data('action', 'add')
                        $('#ClassesModal').modal('toggle');
                        $('#ClassesForm').validate({
                            rules: {
                                grade_class : 'required',
                                name_class : 'required',
                                teacher_class : 'required',
                                stdate_class : 'required',
                            },
                            highlight: function(input) {
                                $(input).parents('.form-line').addClass('error');
                            },
                            unhighlight: function(input) {
                                $(input).parents('.form-line').removeClass('error');
                            },
                            errorPlacement: function(error, element) {
                                $(element).parents('.form-group').append(error);
                            }
                        });
                    }
                },
                {
                    text: 'Edit',
                    className: 'btn btn-primary waves-effect',
                    action: function ( e, dt, node, config ) {
                        if(clasTable.rows({selected: true}).data()['0']){
                            $('#ClassesModal-title').text('Edit Class');
                            $('#ClassesModal_submit').data('action', 'edit')
                            $('#ClassesModal').modal('toggle');
                            $('#ClassesForm').validate({
                                rules: {
                                    grade_class : 'required',
                                    name_class : 'required',
                                    teacher_class : 'required',
                                    stdate_class : 'required',
                                },
                                highlight: function(input) {
                                    $(input).parents('.form-line').addClass('error');
                                },
                                unhighlight: function(input) {
                                    $(input).parents('.form-line').removeClass('error');
                                },
                                errorPlacement: function(error, element) {
                                    $(element).parents('.form-group').append(error);
                                }
                            });

                            var data = clasTable.rows({selected: true}).data();
                            $('#grade_class').val(data['0']['classes']['grade']).trigger('change');
                            $('#name_class').val( data['0']['classes']['class_name'] );
                            $('#teacher_class').val(data['0']['classes']['class_teacher']).trigger('change');
                            $('#stdate_class').val( data['0']['classes']['commenced_date'] );
                            $('#class_id').val( data[0]['DT_RowId'].split("_")[1] );
                            $('.form-line').addClass('focused')
                        }

                    }
                }
            ]
        } );

        var subjectsTable = $('#subjects').DataTable( {
            dom: "Bfrtip",
            responsive: true,
            ajax: {
                url: "<?php echo base_url().'index.php/Sadmin/Dtable/ClassSubjects' ?>",
                data:{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' },
                type: "POST"
            },
            serverSide: true,
            columns: [
                { data: "classes.class_name" },
                { data: "subject.subject_name" },
                { data: "teachers.teacher_in_name" }
            ],
            select: true,
            bFilter: false,
            buttons: [
                {
                    text: 'New',
                    className: 'btn btn-success waves-effect',
                    action: function ( e, dt, node, config ) {
                        $('#SubjectsModal-title').text('Add a Subject to a Class');
                        $('#SubjectsModal_submit').data('action', 'add')
                        $('#SubjectsModal').modal('toggle');
                        $('#SubjectsForm').validate({
                            rules: {
                                classsubjest_class : 'required',
                                classsubjest_subject : 'required',
                                classsubjest_teacher : 'required',
                            },
                            highlight: function(input) {
                                $(input).parents('.form-line').addClass('error');
                            },
                            unhighlight: function(input) {
                                $(input).parents('.form-line').removeClass('error');
                            },
                            errorPlacement: function(error, element) {
                                $(element).parents('.form-group').append(error);
                            }
                        });
                    }
                },
                {
                    text: 'Edit',
                    className: 'btn btn-primary waves-effect',
                    action: function ( e, dt, node, config ) {
                        if(subjectsTable.rows({selected: true}).data()['0']){
                            $('#SubjectsModal-title').text('Edit a Subject of the Class');
                            $('#SubjectsModal_submit').data('action', 'edit')
                            $('#SubjectsModal').modal('toggle');
                            $('#SubjectsForm').validate({
                                rules: {
                                    classsubjest_class : 'required',
                                    classsubjest_subject : 'required',
                                    classsubjest_teacher : 'required',
                                },
                                highlight: function(input) {
                                    $(input).parents('.form-line').addClass('error');
                                },
                                unhighlight: function(input) {
                                    $(input).parents('.form-line').removeClass('error');
                                },
                                errorPlacement: function(error, element) {
                                    $(element).parents('.form-group').append(error);
                                }
                            });

                            var data = subjectsTable.rows({selected: true}).data();
                            $('#classsubjest_class').val(data['0']['class_subjects']['class_id']).trigger('change');
                            $('#classsubjest_subject').val( data['0']['class_subjects']['subject_id'] ).trigger('change');
                            $('#classsubjest_teacher').val(data['0']['class_subjects']['teacher_id']).trigger('change');
                            $('#class_subj_id').val( data[0]['DT_RowId'].split("_")[1] );
                            $('.form-line').addClass('focused')
                        }

                    }
                },
                {
                    text: 'Delete',
                    className: 'btn btn-warning waves-effect',
                    action: function ( e, dt, node, config ) {
                        if(subjectsTable.rows({selected: true}).data()['0']){

                            var data = subjectsTable.rows({selected: true}).data();
                            var class_subj_id = data[0]['DT_RowId'].split("_")[1];
                            var formAction = 'delete';
                            var form_data = new FormData();
                            var post_url = "index.php/sadmin/ClassSubjects/2";

                            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
                            form_data.append('formAction', formAction);
                            form_data.append('class_subj_id', class_subj_id);

                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>" + post_url,
                                dataType :'text',
                                data: form_data,
                                contentType: false,
                                processData: false,
                                success: function(response){
                                    location.reload();
                                },
                                error: function (response) {
                                    alert("Error Updating! Please try again.");
                                }
                            });
                        }

                    }
                }
            ]
        } );


        var usersTable = $('#users').DataTable( {
            dom: "Bfrtip",
            responsive: true,
            ajax: {
                url: "<?php echo base_url().'index.php/Sadmin/Dtable/Users' ?>",
                data:{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' },
                type: "POST"
            },
            serverSide: true,
            columns: [
                { data: "name" },
                { data: "uname" },
                { data: "role" }
            ],
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        var role;
                        if(data == '1'){
                            role = "School Coordinator";
                        }else if(data == '2'){
                            role = "Finance Administrator";
                        }else if(data == '3'){
                            role = "Class Teacher";
                        }else {
                            role = "";
                        }
                        return role;
                    },
                    "targets": 2
                }
            ],
            select: true,
            buttons: [
                // { extend: "edit",   editor: usersEditor },
                {
                    text: 'Edit',
                    className: 'btn btn-primary waves-effect',
                    action: function ( e, dt, node, config ) {
                        if(usersTable.rows({selected: true}).data()['0']){
                            $('#userModal').modal('toggle');
                            $('#userEditForm').validate({
                                rules: {
                                    users_name : 'required',
                                    users_username : 'required',
                                },
                                highlight: function(input) {
                                    $(input).parents('.form-line').addClass('error');
                                },
                                unhighlight: function(input) {
                                    $(input).parents('.form-line').removeClass('error');
                                },
                                errorPlacement: function(error, element) {
                                    $(element).parents('.form-group').append(error);
                                }
                            });
                            $.validator.addMethod("PASSWORD",function(value,element){
                                return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,25}$/i.test(value);
                            },"Passwords are 8-25 characters with uppercase letters, lowercase letters and at least one number.");

                            $('#user_passwdResetForm').validate({
                                rules: {
                                    user_passwd : {required :true, PASSWORD: true},
                                    user_repasswd: { equalTo: "#user_passwd" }
                                },
                                highlight: function(input) {
                                    $(input).parents('.form-line').addClass('error');
                                },
                                unhighlight: function(input) {
                                    $(input).parents('.form-line').removeClass('error');
                                },
                                errorPlacement: function(error, element) {
                                    $(element).parents('.form-group').append(error);
                                }
                            });

                            var data = usersTable.rows({selected: true}).data();
                            $('#users_name').val(data['0']['name']);
                            $('#users_username').val( data['0']['uname'] );
                            $('#users_role').val(data['0']['role']).trigger('change');
                            $('.users_user_id').val( data[0]['DT_RowId'].split("_")[1] );
                            $('.form-line').addClass('focused')

                            if (data['0']['role'] == '1') {
                              $('#users_role').prop('disabled', true);
                            } else {
                              $('#users_role').prop('disabled', false);
                            }
                        }

                    }
                }
            ]
        } );

        $('#ClassesModal_submit').click(function() {
            var formAction = $(this).data('action');
            var form_data = new FormData();

            var grade = $('#grade_class').val();
            var class_name = $('#name_class').val();
            var class_teacher = $('#teacher_class').val();
            var class_startdate = $('#stdate_class').val();
            var class_id = $('#class_id').val();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('formAction', formAction);
            form_data.append('grade_class', grade);
            form_data.append('name_class', class_name);
            form_data.append('teacher_class', class_teacher);
            form_data.append('stdate_class', class_startdate);
            form_data.append('class_id', class_id);

            if($('#ClassesForm').valid()){
                var post_url = "index.php/sadmin/classes/2";
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + post_url,
                    dataType :'text',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        location.reload();
                    },
                    error: function (response) {
                        alert("Error Updating! Please try again.");
                    }
                });
            }

        });

        $('#students').DataTable( {
            dom: "Bfrtip",
            responsive: true,
            ajax: {
                url: "<?php echo base_url().'index.php/Sadmin/Dtable/Students' ?>",
                data:{  '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' },
                type: "POST"
            },
            serverSide: true,
            columns: [
                { data: "students_info.index_no" },
                { data: "students_info.in_name" },
                { data: "students_info.nic" },
                { data: "students_info.gender" },
                { data: "students_info.address" },
                { data: "students_info.telephone" },
                { data: "students_info.medium" },
                { data: "students_info.dist_school" },
                { data: "students_info.income" },
                { data: "travel_mode.travel_mode" }
            ],
            select: true,
            buttons: [
                'copy', 'csv', 'pdf', 'print'
            ]
        } );

        $('#TeachersModal_submit').click(function() {

            var formAction = $(this).data('action');
            var form_data = new FormData();
            var post_url = "index.php/sadmin/Teachers/2";

            var teacher_id = $('#teacher_id').val();
            var title = $('#title').val();
            var teacherName = $('#teacherName').val();
            var teacherNIC = $('#teacherNIC').val();
            var teacherDOB = $('#teacherDOB').val();
            var teacherMobile = $('#teacherMobile').val();
            var teacherEmail = $('#teacherEmail').val();
            var sub1 = $('#sub1').val();
            var trained1 = $('#trained1').val();
            var sub2 = $('#sub2').val();
            var trained2 = $('#trained2').val();
            var sub3 = $('#sub3').val();
            var trained3 = $('#trained3').val();
            var teacherApp_ser = $('#teacherApp_ser').val();
            var teacherApp_sch = $('#teacherApp_sch').val();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('formAction', formAction);
            form_data.append('teacher_id', teacher_id);
            form_data.append('title', title);
            form_data.append('teacherName', teacherName);
            form_data.append('teacherNIC', teacherNIC);
            form_data.append('teacherDOB', teacherDOB);
            form_data.append('teacherMobile', teacherMobile);
            form_data.append('teacherEmail', teacherEmail);
            form_data.append('sub1', sub1);
            form_data.append('trained1', trained1);
            form_data.append('sub2', sub2);
            form_data.append('trained2', trained2);
            form_data.append('sub3', sub3);
            form_data.append('trained3', trained3);
            form_data.append('teacherApp_ser', teacherApp_ser);
            form_data.append('teacherApp_sch', teacherApp_sch);

            if($('#TeachersForm').valid()){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + post_url,
                    dataType :'text',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        location.reload();
                    },
                    error: function (response) {
                        alert("Error Updating! Please try again.");
                    }
                });
            }
        });

        $('#SubjectsModal_submit').click(function() {
            var formAction = $(this).data('action');
            var form_data = new FormData();

            var classsubjest_class = $('#classsubjest_class').val();
            var classsubjest_subject = $('#classsubjest_subject').val();
            var classsubjest_teacher = $('#classsubjest_teacher').val();
            var class_subj_id = $('#class_subj_id').val();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('formAction', formAction);
            form_data.append('classsubjest_class', classsubjest_class);
            form_data.append('classsubjest_subject', classsubjest_subject);
            form_data.append('classsubjest_teacher', classsubjest_teacher);
            form_data.append('class_subj_id', class_subj_id);

            if($('#ClassesForm').valid()){
                var post_url = "index.php/sadmin/ClassSubjects/2";
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + post_url,
                    dataType :'text',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        location.reload();
                    },
                    error: function (response) {
                        alert("Error Updating! Please try again.");
                    }
                });
            }

        });

        $('.users_submit').click(function() {
            var formAction = $(this).data('action');
            var form_data = new FormData();

            var users_user_id = $('#users_user_id').val();
            var users_name = $('#users_name').val();
            var users_username = $('#users_username').val();
            var users_role = $('#users_role').val();
            var user_passwd = $('#user_passwd').val();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('formAction', formAction);
            form_data.append('users_user_id', users_user_id);
            form_data.append('users_name', users_name);
            form_data.append('users_username', users_username);
            form_data.append('users_role', users_role);
            form_data.append('user_passwd', user_passwd);

            if (formAction == 'details') {
              formValid = $('#userEditForm').valid();
            } else {
              formValid = $('#user_passwdResetForm').valid();
            }

            if(formValid){
                var post_url = "index.php/sadmin/users/2";
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>" + post_url,
                    dataType :'text',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    success: function(response){
                        location.reload();
                    },
                    error: function (response) {
                        location.reload();
                    }
                });
            }

        });


        function getTeachers() {
            var dataarray = {'<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>'};
            var post_url = "index.php/FormControl/getTeachers_School";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: dataarray,
                async: false,
                success: function(res){
                    $('.teacher_list').empty();
                    $('.teacher_list').append('<option value="" hidden selected> ---------Please Select---------</option>');
                    $.each(res, function(ID){
                        $('.teacher_list').append('<option value='+res[ID].id+'>'+res[ID].teacher_in_name+'</option>');
                    });
                    $('.teacher_list').selectpicker('refresh');
                }
            });
        }

    });
</script>
