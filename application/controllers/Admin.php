<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# @Author: Kosala Gangabadage <kosala>
# @Date:   2017-10-25T14:50:29+05:30
# @Email:  kosala4@gmail.com
# @Last modified by:   Kosala Gangabadage
# @Last modified time: 2018-05-23T14:20:35+05:30


class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model'); //load database model.
        $this->load->model('Form_data_model'); //load database model.
        $this->load->model('Report_data_model'); //load database model.
    }

    public $response = array("result"=>"none", "data"=>"none");

    public function index()
    {
        $this->check_sess();
        $this->load->view('head');
        $this->load->view('admin/sidebar');

        $this->response['schools'] = $this->Form_data_model->select('schools');
        $this->response['coordinators'] = $this->Form_data_model->select('coordinators');
        $this->response['province'] = $this->Form_data_model->select('province');
        $this->response['zones'] = $this->Form_data_model->select('zones');
        $this->response['padmins'] = $this->Form_data_model->getPadmins();
        $this->response['zadmins'] = $this->Form_data_model->getZadmins();
        $this->load->view('admin/dashboard', $this->response);
        $this->load->view('footer');
    }

    public function reports()
    {
        $this->check_sess();
        $this->load->view('head');

        $this->response['subjects'] = $this->Form_data_model->select('subjects');
        $this->response['province'] = $this->Form_data_model->select('province');

        $this->load->view('admin/reports-sidebar', $this->response);
        $this->load->view('general/reports', $this->response);
        $this->load->view('footer');
    }

    public function school($school_id, $school_name)
    {
        $this->check_sess();
        $this->load->view('head');

        $this->response['school_id'] = $school_id;
        $this->response['school_name'] = rawurldecode($school_name);
        $this->response['classes'] = $this->Report_data_model->getSchoolClasses($school_id, 'count');
        $this->response['teachers'] = $this->Report_data_model->getSchoolTeachers($school_id, 'count');
        $this->response['students'] = $this->Report_data_model->getSchoolStudents($school_id, 'count', 'all');

        $this->load->view('admin/reports-sidebar', $this->response);
        $this->load->view('general/school', $this->response);
        $this->load->view('footer');
    }

    public function check_sess()
    {
        if ($this->session->user_logged == "in") {

        } elseif ($this->session->user_role == '0'){

        } else {
            $this->logout(); //Redirect to login page if session not initiated.
        }
    }

    //Logout function
    function logout()
    {
        $this->session->sess_destroy();
        redirect('/login/index');
    }

    //Function to view add new school Form.
    function addSchool()
    {
        $this->check_sess();
        $this->load->view('head');
        $this->load->view('admin/sidebar');

        $this->response['province'] = $this->Form_data_model->select('province');
        $this->load->view('admin/addSchool', $this->response);
        $this->load->view('footer');
    }

    //Function to add new school.
    function addNewSchool()
    {
        $census_id = $this->security->xss_clean($_REQUEST['census_id']);
        $name = $this->security->xss_clean($_REQUEST['name']);
        $province_id = $this->security->xss_clean($_REQUEST['province']);
        $district_id = $this->security->xss_clean($_REQUEST['district']);
        $zone_id = $this->security->xss_clean($_REQUEST['zone']);
        $telephone = $this->security->xss_clean($_REQUEST['telephone']);
        $fax = $this->security->xss_clean($_REQUEST['fax']);
        $email = $this->security->xss_clean($_REQUEST['email']);
        $pname = $this->security->xss_clean($_REQUEST['pname']);
        $pmobile = $this->security->xss_clean($_REQUEST['pmobile']);
        $pemail = $this->security->xss_clean($_REQUEST['pemail']);

        $schoolArray = array('census_id' =>$census_id, 'schoolname' => $name, 'province_id' => $province_id, 'district_id' => $district_id, 'zone_id' => $zone_id, 'telephone' => $telephone, 'fax' => $fax, 'email' => $email, 'principal_name' => $pname, 'principal_mobile' => $pmobile, 'principal_email' => $pemail);

        $res = $this->Form_data_model->insert('schools', $schoolArray);
        //$res = 1;
        if ($res == 1){

            $this->session->set_flashdata('success',$name . ' School Added Successfully');
            redirect('admin/index');

        } else {
            $this->session->set_flashdata('not-success','Something went wrong! School Did not Added');
            redirect('admin/index');
        }
    }

    public function Subjects()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        // Datatables Variables

        //Load our library EditorLib
        $this->load->library('EditorLib');

        //Call the process method to process the posted data
        $this->editorlib->process($_POST);

        //Let the model produce the data
        $this->editorlib->CI->DataTable_model->Subjects($_POST);
    }

    public function Schools()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $formAction = $this->security->xss_clean($this->input->post('formAction'));
        $census_id = $this->security->xss_clean($this->input->post('census_id'));
        $name = $this->security->xss_clean($this->input->post('name'));
        $province_id = $this->security->xss_clean($this->input->post('province'));
        $district_id = $this->security->xss_clean($this->input->post('district'));
        $zone_id = $this->security->xss_clean($this->input->post('zone'));
        $telephone = $this->security->xss_clean($this->input->post('telephone'));
        $fax = $this->security->xss_clean($this->input->post('fax'));
        $email = $this->security->xss_clean($this->input->post('email'));
        $pname = $this->security->xss_clean($this->input->post('pname'));
        $pmobile = $this->security->xss_clean($this->input->post('pmobile'));
        $pemail = $this->security->xss_clean($this->input->post('pemail'));
        $lat = $this->security->xss_clean($this->input->post('lat'));
        $lot = $this->security->xss_clean($this->input->post('lot'));

        $schoolArray = array('census_id' =>$census_id, 'schoolname' => $name, 'province_id' => $province_id, 'district_id' => $district_id, 'zone_id' => $zone_id, 'telephone' => $telephone, 'fax' => $fax, 'email' => $email, 'principal_name' => $pname, 'principal_mobile' => $pmobile, 'principal_email' => $pemail, 'lat' => $lat, 'lot' => $lot);


        if ($formAction == 'add') {
            $res = $this->Form_data_model->insert('schools', $schoolArray);
        } else if ($formAction == 'edit') {
            $res = $this->Form_data_model->update('schools', 'census_id', $census_id, $schoolArray);
        } else if ($formAction == 'delete') {
            $res = $this->Form_data_model->deleteStudent( $std_id );
        }

        if ($res == 1){

            $this->session->set_flashdata('success',$name . ' School Added Successfully');
            redirect('admin/index');

        } else {
            $this->session->set_flashdata('not-success','Something went wrong! School Did not Added');
            redirect('admin/index');
        }

    }

    public function Users()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $formAction = $this->security->xss_clean($_REQUEST['action']);
        $u_id = $this->security->xss_clean($_REQUEST['u_id']);
        $role = $this->security->xss_clean($_REQUEST['role']);
        $p_id = $this->security->xss_clean($_REQUEST['p_id']);
        $z_id = $this->security->xss_clean($_REQUEST['z_id']);
        $ufname = $this->security->xss_clean($_REQUEST['ufname']);
        $uname = $this->security->xss_clean($_REQUEST['uname']);
        $passwd = password_hash($this->security->xss_clean($_REQUEST['passwd']), PASSWORD_DEFAULT);

        $userArray = array('name' =>$ufname, 'uname' => $uname, 'role' => $role, 'province_id' => $p_id, 'zone_id' => $z_id);

        if ($formAction == 'add') {
            $userArray['passwd'] = $passwd;
            $res = $this->Form_data_model->insert('user', $userArray);
        } else if ($formAction == 'edit') {
            if ($_REQUEST['passwd'] != '') {
                $userArray['passwd'] = $passwd;
            }
            $res = $this->Form_data_model->update('user', 'id', $u_id, $userArray);
        } else if ($formAction == 'delete') {
            $res = $this->Form_data_model->delete('user', 'id', $u_id);
        }

        if ($res == 1){

            $this->session->set_flashdata('success','Successful!');
            redirect('admin/index');

        } else {
            $this->session->set_flashdata('not-success','Something went wrong!');
            redirect('admin/index');
        }

    }

    public function changeCoordinator()
    {
        header('Content-Type: application/x-json; charset=utf-8');

        $formAction = $this->security->xss_clean($this->input->post('formAction'));
        $school_id = $this->security->xss_clean($this->input->post('school_id'));
        $cname = $this->security->xss_clean($this->input->post('cname'));
        $cnic = $this->security->xss_clean($this->input->post('cnic'));
        $cdob = $this->security->xss_clean($this->input->post('cdob'));
        $cmobile = $this->security->xss_clean($this->input->post('cmobile'));
        $cemail = $this->security->xss_clean($this->input->post('cemail'));
        $appsch = $this->security->xss_clean($this->input->post('appsch'));
        $appser = $this->security->xss_clean($this->input->post('appser'));
        $cuname = strtolower($this->security->xss_clean($this->input->post('cuname')));
        $cur_cuname = strtolower($this->security->xss_clean($this->input->post('cur_cuname')));
        $cpw = password_hash($this->security->xss_clean($this->input->post('cpw')), PASSWORD_DEFAULT);
        $cID = $this->security->xss_clean($this->input->post('cID'));
        $uID = $this->security->xss_clean($this->input->post('uID'));

        $coordinator_array = array('school_id' => $school_id, 'coordinator_nic' => $cnic, 'coordinator_name' => $cname, 'coordinator_dob' => $cdob, 'coordinator_mobile' => $cmobile, 'coordinator_email' => $cemail, 'coordinator_ser_app' => $appser, 'coordinator_sch_app' => $appsch);

        $user_array = array('role' => '1', 'name' => $cname, 'uname' => $cuname, 'passwd' => $cpw, 'school_id' => $school_id);

        $UIDArray = $this->Form_data_model->get_recent_id('user');
        $newUId = $UIDArray['0']['id'] + 1;// + 1
        if ($formAction == 'add') {
            $user_array['id'] = $newUId;
            $coordinator_array['user_id'] = $newUId;
            $res = $this->Form_data_model->addCoordinator($coordinator_array, $user_array);
        } else if ($formAction == 'edit') {
            //$coordinator_array['id'] = $cID;
            if ($cuname) {
                if ($this->input->post('cpw') != '') {
                    $user_arrayx = array('role' => '1', 'name' => $cname, 'uname' => $cuname, 'passwd' => $cpw, 'school_id' => $school_id);
                } else {
                    $user_arrayx = array('role' => '1', 'name' => $cname, 'uname' => $cuname, 'school_id' => $school_id);
                }
                $res = $this->Form_data_model->updateCoordinator($coordinator_array, $user_arrayx, $cID, $uID);
            } else {
                $res = $this->Form_data_model->update('coordinators', 'id', $cID, $coordinator_array);
            }

        }

        if($res == '1'){
            $this->session->set_flashdata('success',$cname . ' Coordinator Details Changed Successfully');
        }else {
            $this->session->set_flashdata('not-success','Something went wrong!');
        }
    }

    function sendEmail()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $recepients = $this->security->xss_clean($this->input->post('sel_list'));
        $message = $this->security->xss_clean($this->input->post('message'));
        $subject = $this->security->xss_clean($this->input->post('subject'));

        $this->load->library('email');

        $this->email->from('13years.admin@moe.gov.lk', '13 Years Admin');

        $this->email->to($recepients);

        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();
         echo $message;
    }

    public function editProfile()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $edit = $this->security->xss_clean($this->input->post('edit'));
        $user_id = $this->security->xss_clean($this->input->post('user_id'));
        $in_name = $this->security->xss_clean($this->input->post('in_name'));
        $passwd = password_hash($this->security->xss_clean($this->input->post('passwd')), PASSWORD_DEFAULT);
        $res = '0';

        if ($edit == 'name') {
            $userArray = array('name' =>$in_name);
            $res = $this->Form_data_model->update('user', 'id', $user_id, $userArray);
        } elseif ($edit == 'passwd') {
            $userArray = array('passwd' =>$passwd);
            $res = $this->Form_data_model->update('user', 'id', $user_id, $userArray);
        }
    }
}
