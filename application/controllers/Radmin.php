<?php
defined('BASEPATH') OR exit('No direct script access allowed');
# @Author: Kosala Gangabadage <kosala>
# @Date:   2017-10-25T14:50:29+05:30
# @Email:  kosala4@gmail.com
# @Last modified by:   Kosala Gangabadage
# @Last modified time: 2018-05-23T14:20:35+05:30
# Regional Administrators


class Radmin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model'); //load database model.
        $this->load->model('Regional_data_model'); //load database model.
        $this->load->model('Form_data_model'); //load database model.
        $this->load->model('Report_data_model'); //load database model.
    }
    public function index()
    {
        $this->check_sess();
        $this->load->view('head');
        $this->load->view('radmin/sidebar');

        $this->response['schools'] = $this->Regional_data_model->getSchools();
        $this->response['coordinators'] = $this->Regional_data_model->select('coordinators');
        $this->response['province'] = $this->Regional_data_model->select('province');
        if ($this->session->user_role == '6'){
            $this->response['zadmins'] = $this->Regional_data_model->getZadmins();
        }
        $this->load->view('radmin/dashboard', $this->response);
        $this->load->view('footer');
    }

    public function reports()
    {
        $this->check_sess();
        $this->load->view('head');

        $this->response['subjects'] = $this->Form_data_model->select('subjects');

        $this->load->view('radmin/reports-sidebar', $this->response);
        $this->load->view('general/reports', $this->response);
        $this->load->view('footer');
    }

    public function school($school_id, $school_name)
    {
        $this->check_sess();

        $schoolArray = array('census_id' =>$school_id);
        $school = $this->Form_data_model->searchdb('schools', $schoolArray);
        if ($this->session->user_role == '6'){
            if ($school['0']['province_id'] != $this->session->province) {
                redirect('/radmin/reports');
            }
        } elseif ($this->session->user_role == '7'){
            if ($school['0']['zone_id'] != $this->session->zone) {
                redirect('/radmin/reports');
            }
        }

        $this->load->view('head');


        $this->response['school_id'] = $school_id;
        $this->response['school_name'] = rawurldecode($school_name);
        $this->response['classes'] = $this->Report_data_model->getSchoolClasses($school_id, 'count');
        $this->response['teachers'] = $this->Report_data_model->getSchoolTeachers($school_id, 'count');
        $this->response['students'] = $this->Report_data_model->getSchoolStudents($school_id, 'count', 'all');

        $this->load->view('radmin/reports-sidebar', $this->response);
        $this->load->view('general/school', $this->response);
        $this->load->view('footer');
    }

    public function check_sess()
    {
        if ($this->session->user_logged == "in") {
            //Redirect to login page if session not initiated.
        } elseif ($this->session->user_role == '6'){
            //$this->logout(); //Redirect to login page if user not authored.
        } elseif ($this->session->user_role == '7'){
             //Redirect to login page if user not authored.
        } else {
            $this->logout();
        }
    }

    //Logout function
    function logout()
    {
        $this->session->sess_destroy();
        redirect('/login/index');
    }

}
