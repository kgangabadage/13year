<?php
/**
 * Created by Kosala.
 * email: kosala4@gmail.com
 * User: edu
 * Date: 10/30/17
 * Time: 2:58 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
    .info-box-3-high:hover {
        cursor: pointer;
    }
    .info-box-3:hover {
        cursor: pointer;
    }
    .info-box-3{
        /* border: 2px #fff solid; */
    }
</style>
<link href="<?php echo base_url()."assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"?>" rel="stylesheet" />

<section class="content">
    <div class="container-fluid">

        <!-- Summary Boxes -->
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <h2 id="school_id" data-id="<?php echo $school_id; ?>">
                        SCHOOL SUMMARY - <span -id="schoolName"> <?php echo strtoupper($school_name); ?></span>
                    </h2><small> Click to see the detailed list. </small>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="info-box-3 bg-teal hover-expand-effect col-md-3 DTtrigger" id="schools_count_div" data-type="classes">
                            <div class="icon">
                                <i class="material-icons">business</i>
                            </div>
                            <div class="content">
                                <div class="text">CLASSES</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" id="schools_count" > <?php echo $classes['count']; ?> </div>
                                <div class="text updated hidden">Updated : <span id="update-school"></span> </div>
                            </div>
                        </div>
                        <div class="info-box-3 bg-red hover-expand-effect col-md-3 DTtrigger "data-type="teachers">
                            <div class="icon">
                                <i class="material-icons">people</i>
                            </div>
                            <div class="content">
                                <div class="text">TEACHERS</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" id="teachers_count" > <?php echo $teachers['count']; ?> </div>
                                <div class="text updated hidden">Updated : <span id="update-teachers"></span> </div>
                            </div>
                        </div>
                        <div class="info-box-3 bg-indigo hover-expand-effect col-md-3 DTtrigger" data-type="students">
                            <div class="icon">
                                <i class="material-icons">face</i>
                            </div>
                            <div class="content">
                                <div class="text">STUDENTS</div>
                                <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20" id="classes_count" > <?php echo $students['count']; ?> </div>
                                <div class="text updated hidden">Updated : <span id="update-classes"></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Report Table -->
        <div class="row clearfix">
            <div class="card">
                <div class="header">
                    <h2 id="lists">
                        DETAILED LISTS
                    </h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <table class="table table-bordered table-striped table-hover js-exportable" id="schoolsTable" style="width:100%" >
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- List Selected School info Modal -->
        <div class="modal fade" id="infoModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg2" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title" id="infoModalLabel"> School Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-exportable" id="infoTable" style="width:100%" ></table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect bg-teal" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Student Profile Modal -->
        <div class="modal fade" id="studentProfile" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title" id="studentProfileLabel">Student Profile</h4>
                    </div>
                    <div class="modal-body" style="overflow-y: scroll; max-height:80vh;">
                        <h3> Class Details </h3>
                        <div class="row clearfix">
                            <div class="col-md-6">

                                <div class="col-md-3 form-control-label">
                                    <label for="school">School</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="school" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="grade">Grade</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="grade" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="col-md-3 form-control-label">
                                    <label for="class">Class</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="class" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="class_teacher">Class Teacher</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="class_teacher" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3> Personal Details </h3>
                        <div class="row clearfix">
                            <div class="col-md-6">

                                <div class="col-md-3 form-control-label">
                                    <label for="full_name">Full Name</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="full_name" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="index_no">Index No</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="index_no" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="nic">NIC No</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="nic" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="dob">Birth Day</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="dob" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="gender">Gender</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="gender" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 form-control-label">
                                    <label for="address">Address</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control no-resize " id="address" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="col-md-4 form-control-label">
                                    <label for="telephone">Parent's Telephone No</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="telephone" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 form-control-label">
                                    <label for="medium">Medium</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="medium" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 form-control-label">
                                    <label for="dist_school">Distance to School in km</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="dist_school" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 form-control-label">
                                    <label for="income">Parent's Income</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="income" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 form-control-label">
                                    <label for="travel_mode">Travel Mode</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="travel_mode" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <h3> Attendance History </h3>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover data-table" id="student_attendance">
                                        <thead>
                                            <tr>
                                                <th>Month</th>
                                                <th>Attended Days</th>
                                                <th>Class Days</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="attendance_bar" style="height:auto;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect bg-teal" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/jquery.dataTables.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/jszip.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/select/js/dataTables.select.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/jquery-datatable/extensions/export/buttons.colVis.min.js"?>"></script>
<script src="<?php echo base_url()."assets/plugins/bootstrap-select/js/bootstrap-select.js"?>"></script> <!-- Select Plugin Js -->
<script src="<?php echo base_url()."assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"?>"></script> <!-- Input Mask Plugin Js -->
<script src="<?php echo base_url()."assets/plugins/jquery-countto/jquery.countTo.js"?>"></script>

<script>
    $(document).ready(function () {
        // getTotalDetails();
        // getAllSchools();

        $('#subjectsMenu').addClass('hidden');
        $('#resetMenu').addClass('hidden');
        $('.list').append(' <li> <a href="<?php echo base_url() ; ?>' +  'index.php/' + $('#adminHome').data('admin') + '/reports' + '" > <i class="material-icons">undo</i> <span>Back to Reports</span> </a> </li>');

        $(".filter").click(function(){
            $('li').removeClass('active');
            $(this).parent().addClass('active');
        });

        $('#resetMenu').click(function(){
            location.reload();
        });

        $(document).on('click', '#dt-reset', function(){
            $('.filter-table').val('').change();
        });

        $('.DTtrigger').click(function(){

            var form_data = new FormData();
            var school_id = $('#school_id').data('id');
            var search_type = $(this).data('type');
            var post_url = "index.php/ReportControl/getSchoolData";
            var filter_set = '';

            $('#lists').text( search_type.toUpperCase() + ' LIST');

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('school_id', school_id);
            form_data.append('search_type', search_type);

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(res){
                    if (res.data) {
                        if ( $.fn.dataTable.isDataTable('#schoolsTable')){
                            $('#schoolsTable').DataTable().destroy();
                            $('#schoolsTable').empty();
                        }

                        var table = $('#schoolsTable').DataTable({
                            dom: '<"top"if><"#dt-filter.mb-10">rBt<"bottom"lpF><"clear">',
                            destroy: true,
                            bSort: false,
                            responsive: true,
                            data: res.data,
                            columns: res.columns,
                            columnDefs: [
                                {width: '5%', targets: 0}
                            ],
                            order: [[ 1, 'asc' ]],
                            buttons: {
                                dom: {
                                    button: {
                                        tag: 'button',
                                        className: ''
                                    }
                                }
                            },
                            buttons: [
                                {
                                    extend: 'csv',
                                    text: 'csv',
                                    className: 'btn btn-link waves-effect bg-light-blue',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    title: $('#lists').text() + ' - ' + '<?php echo strtoupper($school_name); ?>',
                                    messageTop: function() {
                                        filter_set = '';
                                        $('#dt-filter').children('select').each(function () {
                                            filter_set += $(this).data('name') + ': ' + this.value + ', ';
                                        });
                                        return 'Filter By: ' + filter_set;
                                     },
                                },
                                {
                                    extend: 'excel',
                                    text: 'excel',
                                    className: 'btn btn-link waves-effect bg-green',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    title: $('#lists').text() + ' - ' + '<?php echo strtoupper($school_name); ?>',
                                    messageTop: function() {
                                        filter_set = '';
                                        $('#dt-filter').children('select').each(function () {
                                            filter_set += $(this).data('name') + ': ' + this.value + ', ';
                                        });
                                        return 'Filter By: ' + filter_set;
                                     },
                                },
                                {
                                    extend: 'pdfHtml5',
                                    text: 'pdf',
                                    className: 'btn btn-link waves-effect bg-deep-orange',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    title: $('#lists').text() + ' - ' + '<?php echo strtoupper($school_name); ?>',
                                    messageTop: function() {
                                        filter_set = '';
                                        $('#dt-filter').children('select').each(function () {
                                            filter_set += $(this).data('name') + ': ' + this.value + ', ';
                                        });
                                        return 'Filter By: ' + filter_set;
                                     },
                                },
                                {
                                    extend: 'print',
                                    text: 'Print',
                                    className: 'btn btn-link waves-effect bg-deep-purple',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    autoPrint: true,
                                    title: $('#lists').text() + ' - ' + '<?php echo strtoupper($school_name); ?>',
                                    messageTop: function() {
                                        filter_set = '';
                                        $('#dt-filter').children('select').each(function () {
                                            filter_set += $(this).data('name') + ': ' + this.value + ', ';
                                        });
                                        return 'Filter By: ' + filter_set;
                                     },
                                }
                            ]
                        });

                        // Add Column filters to the table.
                        switch (search_type) {
                            case 'classes':
                                dtFilter([1]);
                                break;
                            case 'teachers':
                                dtFilter([3, 4, 5]);
                                break;
                            case 'students':
                                dtFilter([4, 5]);
                                table.column(-1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                    cell.innerHTML = '<a class="std_profile" data-id="'+ cell.innerHTML +'" href="javascript:void(0);">View Profile</a>';
                                } );
                                table.column(-1).header().innerHTML = '';
                                break;

                        }

                        table.column(0).header().innerHTML = 'ID';
                        table.columns.adjust().draw();
                        table.on( 'order.dt search.dt', function () {
                            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                                table.cell(cell).invalidate('dom');
                            } );
                        } ).columns.adjust().draw();
                    }
                },
                error: function (response) {

                }
            });
        });

        $('#list-all-schools').click(function(){
            var form_data = new FormData();
            var phase = $(this).data("phase");

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('phase', phase);

            var post_url = "index.php/ReportControl/getAllSchools";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(res){
                    if (res.data) {
                        var table = $('#infoTable').DataTable({
                            dom: 'Bfrtip',
                            destroy: true,
                            bSort: false,
                            responsive: true,
                            data: res.data,
                            columns: res.columns,
                            columnDefs: [
                                {
                                    targets: 0,
                                    header: 'ID'
                                },
                                {
                                    targets: 2,
                                    render: function(data, type, row, info) {
                                        return '<a class="school_profile" data-id="'+row['ID']+'" href="#">'+row['School Name']+'</a>';
                                    }
                                }
                            ],
                            order: [[ 1, 'asc' ]],
                            buttons: [
                                {
                                    extend: 'csv',
                                    text: 'csv',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    title: 'All Schools List' ,
                                    messageTop:  ''
                                },
                                {
                                    extend: 'excel',
                                    text: 'excel',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    title: 'All Schools List',
                                },
                                {
                                    extend: 'pdfHtml5',
                                    text: 'pdf',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    title: 'All Schools List',
                                },
                                {
                                    extend: 'print',
                                    text: 'Print',
                                    exportOptions: {
                                        columns: ':visible'
                                    },
                                    autoPrint: true,
                                    title: 'All Schools List',
                                }
                            ]
                        });

                        table.columns.adjust().draw();
                        table.on( 'order.dt search.dt', function () {
                            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                                table.cell(cell).invalidate('dom');
                            } );
                            table.column(0).header().innerHTML = 'ID';
                        } ).columns.adjust().draw();
                    }
                },
                error: function (response) {

                }
            });
            $('#infoModal').modal('show');
        });

        $('#infoModal').on('hidden.bs.modal', function () {
            if ( $.fn.dataTable.isDataTable('#infoTable')){
                $('#infoTable').DataTable().destroy();
                $('#infoTable').empty();
            }
        })

        $('.getSchool').click(function(){

            $('li').removeClass('active');
            $(this).parent().addClass('active');

            var school_id = $(this).data('id');
            var school_name = $(this).data('name');
            var zone = $(this).data('zone');
            var province = $(this).data('province');

            setSchoolData(school_id, school_name, zone, province);
        });

        $('.searchBySubject').click(function(){
            $('.updated').removeClass('hidden');
            $('#funds_count_div').addClass('hidden');

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            $('#subjectsMenu').parent().addClass('active');

            var name = $(this).data('name');
            $('#summary-header').text(name);
            $('#summary-title').text('');

            var form_data = new FormData();
            var subject_id = $(this).data('id');

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('subject_id', subject_id);

            var post_url = "index.php/ReportControl/getSchoolsBySubject/2";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(response){
                    $('#schools_count').text(response['schools']);
                    $('#teachers_count').text(response['teachers']['count']);
                    $('#update-teachers').text(response['teachers']['last_update']);

                    $('#classes_count').text(response['classes']['count']);
                    $('#update-classes').text(response['classes']['last_update']);

                    $('#students_count').text(response['students']['count']);
                    $('#students_count_male').text(response['students']['male']);
                    $('#students_count_female').text(response['students']['female']);
                    $('#update-students').text(response['students']['last_update']);

                    if ( $.fn.dataTable.isDataTable('#schoolsTable')){
                        $('#schoolsTable').DataTable().destroy();
                        $('#schoolsTable').empty();
                    }
                    var table = $('#schoolsTable').DataTable({
                        dom: '<"top"if><"#dt-filter.mb-10">rBt<"bottom"lpF><"clear">',
                        destroy: true,
                        bSort: false,
                        responsive: true,
                        data: response.data,
                        columns: response.columns,columnDefs: [
                            {
                                targets: -1,
                                render: function(data, type, row, info) {
                                    return '<a class="scl_profile" data-id="'+row['ID']+'" href="#">View Profile</a>';
                                }
                            }
                        ],
                        order: [[ 1, 'asc' ]],
                        buttons: [
                            {
                                extend: 'csv',
                                text: 'csv',
                                className: 'btn btn-link waves-effect bg-light-blue',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                title: 'Schools List' ,
                                messageTop:  ''
                            },
                            {
                                extend: 'excel',
                                text: 'excel',
                                className: 'btn btn-link waves-effect bg-green',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                title: 'Schools List',
                            },
                            {
                                extend: 'pdfHtml5',
                                text: 'pdf',
                                className: 'btn btn-link waves-effect bg-deep-orange',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                title: 'Schools List',
                            },
                            {
                                extend: 'print',
                                text: 'Print',
                                className: 'btn btn-link waves-effect bg-deep-purple',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                autoPrint: true,
                                title: 'Schools List',
                            }
                        ]
                    });

                    dtFilter();

                    table.columns.adjust().draw();
                    table.on( 'order.dt search.dt', function () {
                        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            cell.innerHTML = i+1;
                            table.cell(cell).invalidate('dom');
                        } );
                    } ).columns.adjust().draw();
                },
                error: function (response) {
                    alert("Error! Please try again.");
                }
            });
        });

        $('.searchByProvince').click(function(){
            $('.updated').removeClass('hidden');
            $('#funds_count_div').addClass('hidden');

            $('li').removeClass('active');
            $(this).parent().addClass('active');
            $('#subjectsMenu').parent().addClass('active');

            var name = $(this).data('name');
            $('#summary-header').text(name);
            $('#summary-title').text('');

            var form_data = new FormData();
            var province_id = $(this).data('id');

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('province_id', province_id);

            var post_url = "index.php/ReportControl/getSchoolsByProvince/2";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(response){
                    $('#schools_count').text(response['schools']);
                    $('#teachers_count').text(response['teachers']['count']);
                    $('#update-teachers').text(response['teachers']['last_update']);

                    $('#classes_count').text(response['classes']['count']);
                    $('#update-classes').text(response['classes']['last_update']);

                    $('#students_count').text(response['students']['count']);
                    $('#students_count_male').text(response['students']['male']);
                    $('#students_count_female').text(response['students']['female']);
                    $('#update-students').text(response['students']['last_update']);

                    if ( $.fn.dataTable.isDataTable('#schoolsTable')){
                        $('#schoolsTable').DataTable().destroy();
                        $('#schoolsTable').empty();
                    }
                    var table = $('#schoolsTable').DataTable({
                        dom: 'Bfrtip',
                        destroy: true,
                        bSort: false,
                        responsive: true,
                        data: response.data,
                        columns: response.columns,columnDefs: [
                            {
                                targets: -1,
                                render: function(data, type, row, info) {
                                    return '<a class="scl_profile" data-id="'+row['ID']+'" href="#">View Profile</a>';
                                }
                            }
                        ],
                        order: [[ 1, 'asc' ]],
                        buttons: [
                            {
                                extend: 'csv',
                                text: 'csv',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                title: 'Schools List' ,
                                messageTop:  ''
                            },
                            {
                                extend: 'excel',
                                text: 'excel',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                title: 'Schools List',
                            },
                            {
                                extend: 'pdfHtml5',
                                text: 'pdf',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                title: 'Schools List',
                            },
                            {
                                extend: 'print',
                                text: 'Print',
                                exportOptions: {
                                    columns: [0,1,2,3,4,5,6,7]
                                },
                                autoPrint: true,
                                title: 'Schools List',
                            }
                        ]
                    });

                    table.columns.adjust().draw();
                    table.on( 'order.dt search.dt', function () {
                        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                            cell.innerHTML = i+1;
                            table.cell(cell).invalidate('dom');
                        } );
                    } ).columns.adjust().draw();
                },
                error: function (response) {
                    alert("Error! Please try again.");
                }
            });
        });

        $('#searchSubjects').keyup(function(){
            var filter, ul, li, a, i;
            filter = $(this).val().toUpperCase();
            ul = document.getElementById("subjectListMenu");
            li = ul.getElementsByTagName("li");
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";

                }
            }
        });

        $(document).on('click', '.std_profile', function(){
            var std_id = $(this).data('id');
            var form_data = new FormData();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('std_id', std_id);

            var post_url = "index.php/report/getStudentDetails/2";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(response){
                    $('#school').val( response['class'][0]['schoolname'] );
                    $('#grade').val( response['class'][0]['grade'] );
                    $('#class').val( response['class'][0]['class_name'] );
                    $('#class_teacher').val( response['class'][0]['teacher_in_name'] );

                    $('#studentProfileLabel').text( response['details'][0]['in_name'] + ' Profile' );;
                    $('#full_name').val( response['details'][0]['full_name'] );
                    $('#index_no').val( response['details'][0]['index_no'] );
                    $('#nic').val( response['details'][0]['nic'] );
                    $('#dob').val( response['details'][0]['dob'] );
                    $('#gender').val( response['details'][0]['gender'] );

                    $('#address').val( response['details'][0]['address'] );
                    $('#telephone').val( response['details'][0]['telephone'] );
                    $('#medium').val( response['details'][0]['medium'] );
                    $('#dist_school').val( response['details'][0]['dist_school'] );
                    $('#income').val( response['details'][0]['income'] );
                    $('#travel_mode').val( response['details'][0]['travel_mode'] );

                    var table = $('#student_attendance').DataTable({
                        dom: 't',
                        destroy: true,
                        bSort: false,
                        responsive: true,
                        data: response['attendance'],
                        columns: [
                            { data: 'Month' },
                            { data: 'Attended Days' },
                            { data: 'Class Days' }
                        ],
                        columnDefs: [
                            {width: '10%', targets: 0}
                        ],
                    });
                    table.columns.adjust().draw();

                    var attArray = [];
                    attArray[0] = ['Month', 'Attended days', 'Class days'];
                    $.each( response['attendance'], function( key, value ) {
                        var valuesArray = [];
                        $.each( value, function( k, val ) {
                            if (k != 'Month') {
                                valuesArray.push(parseInt(val));
                            } else{
                                valuesArray.push(val);
                            }

                        });
                        attArray.push(valuesArray);
                    });

                    $('#infoModal').modal('hide');
                    $('#studentProfile').modal('show');
                    //drawAttChart();

                    $('#studentProfile').on('shown.bs.modal', function () {
                        google.charts.load('current', { 'packages': ['corechart']});
                        google.charts.setOnLoadCallback(drawAttChart);

                        function drawAttChart() {
                            var data = google.visualization.arrayToDataTable(attArray);

                            var options = {
                                legend: {position: 'top', alignment: 'start'},
                                title: '',
                                //width:'550',
                                chartArea: {width: '75%'},
                                hAxis: {
                                title: 'Number of Days',
                                ticks: [5,10,15,20,25,30] ,
                                minValue: 0
                                },
                                vAxis: {
                                title: ''
                                }
                            };

                            var chart = new google.visualization.BarChart(document.getElementById('attendance_bar'));
                            //chart.clearChart();
                            chart.draw(data, options);
                        }
                    });
                },
                error: function (response) {
                    alert("Error! Please try again.");
                }
            });
        });

        function getAllSchools(){

            var form_data = new FormData();
            var post_url = "index.php/ReportControl/getAllSchools";
            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(res){
                    if (res.data) {
                        var table = $('#schoolsTable').DataTable({
                            dom: '<"top"if><"#dt-filter.mb-10">rBt<"bottom"lpF><"clear">',
                            destroy: true,
                            bSort: false,
                            responsive: true,
                            data: res.data,
                            columns: res.columns,
                            columnDefs: [
                                {
                                    targets: -1,
                                    render: function(data, type, row, info) {
                                        return '<a class="scl_profile" data-id="'+row['ID']+'" href="#">View Profile</a>';
                                    },
                                    'printable': false
                                }
                            ],
                            order: [[ 1, 'asc' ]],
                            buttons: {
                                dom: {
                                    button: {
                                        tag: 'button',
                                        className: ''
                                    }
                                }
                            },
                            buttons: [
                                {
                                    extend: 'csv',
                                    text: 'csv',
                                    className: 'btn btn-link waves-effect bg-light-blue',
                                    exportOptions: {
                                        columns: [0,1,2,3,4,5,6,7]
                                    },
                                    title: 'Schools List' ,
                                    messageTop:  ''
                                },
                                {
                                    extend: 'excel',
                                    text: 'excel',
                                    className: 'btn btn-link waves-effect bg-green',
                                    exportOptions: {
                                        columns: [0,1,2,3,4,5,6,7]
                                    },
                                    title: 'Schools List',
                                },
                                {
                                    extend: 'pdfHtml5',
                                    text: 'pdf',
                                    className: 'btn btn-link waves-effect bg-deep-orange',
                                    exportOptions: {
                                        columns: [0,1,2,3,4,5,6,7]
                                    },
                                    title: 'Schools List',
                                },
                                {
                                    extend: 'print',
                                    text: 'Print',
                                    className: 'btn btn-link waves-effect bg-deep-purple',
                                    exportOptions: {
                                        columns: [0,1,2,3,4,5,6,7]
                                    },
                                    autoPrint: true,
                                    title: 'Schools List',
                                }
                            ]
                        });
                        dtFilter();
                        table.columns.adjust().draw();
                        table.on( 'order.dt search.dt', function () {
                            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                                cell.innerHTML = i+1;
                                table.cell(cell).invalidate('dom');
                            } );

                        } ).columns.adjust().draw();
                    }
                },
                error: function (response) {

                }
            });
        }

        function getTotalDetails(){

            var form_data = new FormData();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');

            var post_url = "index.php/report/getTotalDetails/2";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(response){
                    $('#schools_count').text(response['schools']);
                    $('#teachers_count').text(response['teachers']);
                    $('#classes_count').text(response['classes']);
                    $('#students_count').text(response['students']);
                    $('#students_count_male').text(response['students_male']);
                    $('#students_count_female').text(response['students_female']);
                },
                error: function (response) {
                    alert("Error! Please try again.");
                }
            });
        }

        function setSchoolData(school_id, school_name, zone, province){
            $('.updated').removeClass('hidden');
            $('#funds_count_div').removeClass('hidden');
            $('#schools_count_div').addClass('hidden');
            $('#schoolMenu').parent().addClass('active');

            $('#summary-header').text(school_name);
            $('#summary-title').text(zone + ' Zone, ' + province + ' Province');

            var post_url = "index.php/report/getschoolData/2";

            var form_data = new FormData();

            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash(); ?>');
            form_data.append('school_id', school_id);

            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>" + post_url,
                dataType :'json',
                data: form_data,
                contentType: false,
                processData: false,
                success: function(response){
                    $('#teachers_count').text(response['teachers']['count']);
                    $('#update-teachers').text(response['teachers']['last_update']);

                    $('#classes_count').text(response['classes']['count']);
                    $('#update-classes').text(response['classes']['last_update']);

                    $('#students_count').text(response['students']['count']);
                    $('#students_count_male').text(response['students']['male']);
                    $('#students_count_female').text(response['students']['female']);
                    $('#update-students').text(response['students']['last_update']);

                    $('#funds_count').text((response['funds']['total']).toLocaleString());
                },
                error: function (response) {
                    alert("Error Updating! Please try again.");
                }
            });
        }

        function dtFilter(columns) {
            var table = $('#schoolsTable').DataTable();
            table.columns( columns ).every( function () {
                var that = this;
                var title = $(that.header()).html();
                // var search = '^\\s' + $(this).val() +'\\s*$';
                // Create the select list and search operation
                $('#dt-filter').css('margin-bottom', '10px');
                $('#dt-filter').append('Filter By '+title+': ');
                var select = $('<select class="form-control filter-table" id="filter-'+ title +'" data-name="'+ title +'"/>')
                    .appendTo( '#dt-filter' )
                    .on( 'change', function () {
                        var search1 =  $(this).val();
                        var search = '^' + search1 +'$';
                        if (search1 != '') {
                            that.search( search, true, false ).draw();
                        }else {
                            that.search( $(this).val() ).draw();
                        }

                    } );

                select.append( $('<option value=""> '+title+' </option>') );
                this
                    .cache( 'search' )
                    .sort()
                    .unique()
                    .each( function ( d ) {
                        select.append( $('<option value="'+d+'">'+d+'</option>') );
                    } );

            } );

            $('#dt-filter select').css('margin-right', '40px');
            $('#dt-filter').append('<button type="button" class="btn btn-link waves-effect bg-teal reset" id="dt-reset">RESET TABLE</button>');


        }

    });
</script>
