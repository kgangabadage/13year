<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by Kosala.
 * email: kosala4@gmail.com
 * User: edu
 * Date: 9/28/17
 * Time: 10:28 AM
 */

class Sadmin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model'); //load database model.
        $this->load->model('Form_data_model'); //load database model.
        $this->load->library('dblog');
    }

    public $response = array("result"=>"none", "data"=>"none");

    public function index()
    {
        $this->check_sess();
        $this->load->view('head');
        $this->load->view('sadmin/sidebar');

        $this->getSchoolDetails();
        $this->load->view('sadmin/dashboard', $this->response);
        $this->load->view('footer');
    }

    public function check_sess()
    {
        if ($this->session->user_logged != "in") {
            $this->logout(); //Redirect to login page if session not initiated.
        } elseif ($this->session->user_role != '1'){
            $this->logout(); //Redirect to login page if user not authored.
        }
    }

    //Logout function
    function logout()
    {
        $this->session->sess_destroy();
        redirect('/login/index');
    }

    function getSchoolDetails()
    {
        $SchoolsearchArray = array('census_id' =>$this->session->school_id);
        $this->response['school'] = $this->Form_data_model->searchdb('schools', $SchoolsearchArray);

        $searchArray = array('school_id' =>$this->session->school_id);
        $this->response['teachers'] = $this->Form_data_model->searchdb('teachers', $searchArray );

        $this->response['classes'] = $this->Form_data_model->searchdb('classes', $searchArray );

        $this->response['subjects'] = $this->Form_data_model->select('subjects');
    }

    /*function getSchoolTeachers()
    {
        $searchArray = array('census_id' =>$this->session->school_id);
        $this->response['teachers'] = $this->Form_data_model->searchdb('schools', $searchArray);
    }*/

    //Function to view add new school Form.
    function addTeacher()
    {
        $this->check_sess();
        $this->load->view('head');
        $this->load->view('sadmin/sidebar');

        $this->response['subjects'] = $this->Form_data_model->select('subjects');
        $this->load->view('sadmin/addTeacher', $this->response);
        $this->load->view('footer');
    }

    //Function to add new school.
    function Teachers()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $school_id = $this->session->school_id;
        $formAction = $this->security->xss_clean($_REQUEST['formAction']);
        $teacher_id = $this->security->xss_clean($_REQUEST['teacher_id']);
        $title = $this->security->xss_clean($_REQUEST['title']);
        $name = $this->security->xss_clean($_REQUEST['name']);
        $nic = $this->security->xss_clean($_REQUEST['nic']);
        $dob = $this->security->xss_clean($_REQUEST['dob']);
        $mobile = $this->security->xss_clean($_REQUEST['mobile']);
        $email = $this->security->xss_clean($_REQUEST['email']);

        $trained1 = $this->security->xss_clean($_REQUEST['trained1']);
        $trained2 = (isset($_REQUEST['trained2']) ? $this->security->xss_clean($_REQUEST['trained2']): '0');
        $trained3 = (isset($_REQUEST['trained3']) ? $this->security->xss_clean($_REQUEST['trained3']): '0');

        $sub1 = $this->security->xss_clean($_REQUEST['sub1']);
        $sub2 = ($_REQUEST['sub2'] != "" ? $this->security->xss_clean($_REQUEST['sub2']) : NULL);
        $sub3 = ($_REQUEST['sub3'] != "" ? $this->security->xss_clean($_REQUEST['sub3']) : NULL);

        $app_ser = $this->security->xss_clean($_REQUEST['app_ser']);
        $app_sch = $this->security->xss_clean($_REQUEST['app_sch']);

        $dataArray = array('school_id' =>$school_id, 'title' => $title, 'teacher_in_name' => $name, 'nic' => $nic, 'dob' => $dob, 'teacher_mobile' => $mobile, 'teacher_email' => $email, 'teacher_trained_1' => $trained1, 'teacher_trained_2' => $trained2, 'teacher_trained_3' => $trained3, 'teacher_sub_1' => $sub1, 'teacher_sub_2' => $sub2, 'teacher_sub_3' => $sub3, 'app_date_service' => $app_ser, 'app_date_school' => $app_sch, 'last_edit' => $this->session->user_id );

        if ($formAction == 'add') {
            $res = $this->Form_data_model->insert('teachers', $dataArray);
        } else if ($formAction == 'edit') {
            $res = $this->Form_data_model->update('teachers', 'id', $teacher_id, $dataArray);
        } else if ($formAction == 'delete') {
            $searchArray = array('class_teacher' => $teacher_id, );
            $updateTeacher = array('class_teacher' => NULL, );
            $resSearch = $this->Form_data_model->searchdb('classes', $searchArray);
            if ($resSearch) {
                $res = $this->Form_data_model->update('classes', 'class_teacher', $teacher_id, $updateTeacher);
            }
            $res = $this->Form_data_model->delete( 'teachers', 'id', $teacher_id );
        }

        if($res['code']){
            $error = "Something Went Wrong! - " . $res['message'];
            $this->session->set_flashdata('not-success', $error);
            redirect('sadmin/index');
        } else {
            $this->session->set_flashdata('success',$name . ' Successfully Changed Teacher Details');
            redirect('sadmin/index');
        }


        // $res = $this->Form_data_model->insert('teachers', $dataArray);
        // //$res = 1;
        // if ($res == 1){
        //     $this->session->set_flashdata('success',$name . ' Teacher Has Successfully Added');
        //     redirect('sadmin/index');
        //
        // } else {
        //     $this->session->set_flashdata('not-success','Something went wrong! Teacher Details Did not Added');
        //     redirect('sadmin/index');
        // }
    }

    function createUser()
    {
        $this->check_sess();
        $this->load->view('head');
        $this->load->view('sadmin/sidebar');

        $this->getSchoolDetails();
        $this->load->view('sadmin/addUser', $this->response);
        $this->load->view('footer');
    }

    //Function to add new school.
    function createNewUser()
    {
        $this->check_sess();

        header('Content-Type: application/x-json; charset=utf-8');

        $school_id = $this->session->school_id;
        $role = $this->security->xss_clean($this->input->post('role'));
        $teacher_id = $this->security->xss_clean($this->input->post('teacher_id'));
        $teacher_name = $this->security->xss_clean($this->input->post('teacher_name'));
        $in_name = $this->security->xss_clean($this->input->post('in_name'));
        $u_name = strtolower($this->security->xss_clean($this->input->post('u_name')));
        $password = password_hash($this->security->xss_clean($this->input->post('password')), PASSWORD_DEFAULT);

        $name = ($role == '3' ? $teacher_name : $in_name);
        $teacher_id = ($role == '3' ? $teacher_id : NULL);

        $user_array = array('role' => $role, 'name' => $name, 'uname' => $u_name, 'passwd' => $password, 'teacher_id' => $teacher_id, 'school_id' => $school_id);

        $res = $this->Form_data_model->insert('user', $user_array);

        if($res == '1'){
            echo "success";
        }else {
            echo "not success";
        }
    }

    //Function to Add / Edit Class
    public function Classes()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $formAction = $this->security->xss_clean($this->input->post('formAction'));
        $census_id = $this->session->school_id;
        $grade_class = $this->security->xss_clean($this->input->post('grade_class'));
        $name_class = $this->security->xss_clean($this->input->post('name_class'));
        $teacher_class = $this->security->xss_clean($this->input->post('teacher_class'));
        $stdate_class = $this->security->xss_clean($this->input->post('stdate_class'));
        $class_id = $this->security->xss_clean($this->input->post('class_id'));

        $classArray = array('school_id' =>$census_id, 'grade' => $grade_class, 'class_name' => $name_class, 'class_teacher' => $teacher_class, 'commenced_date' => $stdate_class);

        if ($formAction == 'add') {
            $res = $this->Form_data_model->insert('classes', $classArray);
        } else if ($formAction == 'edit') {
            $res = $this->Form_data_model->update('classes', 'id', $class_id, $classArray);
        } else if ($formAction == 'delete') {
            //$res = $this->Form_data_model->deleteStudent( $std_id );
        }

        if ($res == 1){
            $this->session->set_flashdata('success',$name . ' Class Added Successfully');
        } else {
            $this->session->set_flashdata('not-success','Something went wrong! Class Did not Added');
        }

    }

    //Function to Add / Edit Class Subjects
    public function ClassSubjects()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $formAction = $this->security->xss_clean($this->input->post('formAction'));
        $census_id = $this->session->school_id;
        $class = $this->security->xss_clean($this->input->post('classsubjest_class'));
        $subject = $this->security->xss_clean($this->input->post('classsubjest_subject'));
        $teacher = $this->security->xss_clean($this->input->post('classsubjest_teacher'));
        $subj_id = $this->security->xss_clean($this->input->post('class_subj_id'));

        $subjectArray = array('school_id' =>$census_id, 'class_id' => $class, 'subject_id' => $subject, 'teacher_id' => $teacher);

        if ($formAction == 'add') {
            $res = $this->Form_data_model->insert('class_subjects', $subjectArray);
        } else if ($formAction == 'edit') {
            $res = $this->Form_data_model->update('class_subjects', 'id', $subj_id, $subjectArray);
        } else if ($formAction == 'delete') {
            $res = $this->Form_data_model->delete( 'class_subjects', 'id', $subj_id );
        }

        if($res['code']){
            $error = "Something Went Wrong! - " . $res['message'];
            $this->session->set_flashdata('not-success', $error);
        } else {
            $this->session->set_flashdata('success',$name . ' - Successfully Changed User Data');
        }

    }

    //Function to Add / Edit Class
    public function Users()
    {
        header('Content-Type: application/x-json; charset=utf-8');
        $formAction = $this->security->xss_clean($this->input->post('formAction'));
        $census_id = $this->session->school_id;
        $user_id = $this->security->xss_clean($this->input->post('users_user_id'));
        $name = $this->security->xss_clean($this->input->post('users_name'));
        $username = $this->security->xss_clean($this->input->post('users_username'));
        $role = $this->security->xss_clean($this->input->post('users_role'));
        $passwd = $this->security->xss_clean($this->input->post('user_passwd'));

        $userArray = array('name' => $name, 'uname' => $username, 'role' => $role);
        $passwdArray = array('passwd' => $passwd);

        if ($formAction == 'details') {
            $res = $this->Form_data_model->update('user', 'id', $user_id, $userArray);
        } else if ($formAction == 'password') {
            $res = $this->Form_data_model->update('user', 'id', $user_id, $passwdArray );
        }

        if($res['code']){
            $error = "Something Went Wrong! - " . $res['message'];
            if ($res['code'] == '1062') {
              $message = $res['message'];
              if (stripos($message, "uname") !== false) {
                  $error = "Duplicate User Name! Please use user's NIC no to avoid this error!" ;
              }

            }
            $this->session->set_flashdata('not-success', $error);
        } else {
            $this->session->set_flashdata('success',$name . ' - User Details Edited Successfully');
        }

    }

    public function Dtable($method)
    {
        $this->check_sess();

        header('Content-Type: application/x-json; charset=utf-8');
        // Datatables Variables

        //Load our library EditorLib
        $this->load->library('EditorLib');

        //`Call the process method to process the posted data
        $this->editorlib->process($_POST);

        //Let the model produce the data
        $this->editorlib->CI->DataTable_model->$method($_POST);
    }
}
